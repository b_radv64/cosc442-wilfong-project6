// Generated by Selenium IDE
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import java.util.*;
import java.net.MalformedURLException;
import java.net.URL;
public class SuccessfulLoginTest {
  private WebDriver driver;
  private Map<String, Object> vars;
  JavascriptExecutor js;
  @Before
  public void setUp() {
    driver = new ChromeDriver();
    js = (JavascriptExecutor) driver;
    vars = new HashMap<String, Object>();
  }
  @After
  public void tearDown() {
    driver.quit();
  }
  public String waitForWindow(int timeout) {
    try {
      Thread.sleep(timeout);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    Set<String> whNow = driver.getWindowHandles();
    Set<String> whThen = (Set<String>) vars.get("window_handles");
    if (whNow.size() > whThen.size()) {
      whNow.removeAll(whThen);
    }
    return whNow.iterator().next();
  }
  @Test
  public void successfulLogin() {
    driver.get("https://phptravels.com/demo");
    driver.manage().window().setSize(new Dimension(1296, 696));
    js.executeScript("window.scrollTo(0,1.3333333730697632)");
    vars.put("window_handles", driver.getWindowHandles());
    driver.findElement(By.cssSelector(".wow:nth-child(2) .btn")).click();
    vars.put("win6528", waitForWindow(2000));
    driver.switchTo().window(vars.get("win6528").toString());
    driver.findElement(By.cssSelector(".dropdown-login > #dropdownCurrency")).click();
    driver.findElement(By.linkText("Login")).click();
    driver.findElement(By.cssSelector(".form-group:nth-child(1) > .pure-material-textfield-outlined > span")).click();
    driver.findElement(By.name("username")).sendKeys("user@phptravels.com");
    driver.findElement(By.name("password")).sendKeys("demouser");
    driver.findElement(By.cssSelector(".btn-lg")).click();
    assertThat(driver.findElement(By.cssSelector(".text-align-left")).getText(), is("Hi, Demo User"));
    driver.findElement(By.cssSelector(".dropdown-login > #dropdownCurrency")).click();
    driver.findElement(By.linkText("Logout")).click();
  }
}
